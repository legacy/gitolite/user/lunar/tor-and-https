# “Tor and HTTPS”
# Copyright © 2012 Electronic Frontier Foundation
#           © 2014 Lunar <lunar@torproject.org>
#           © 2014 The Tor Project, Inc.
# Licensed under CC BY 3.0

PROJECT = tor-and-https
LINGUAS = en fr de pt es it ro ar nl da nb th uk pl cs zh_CN tr el ca ja fi km lv sv bg fa hr_HR hu nn ru sk zh_TW lt
POTFILE = $(PROJECT).pot
POFILES = $(foreach lc,$(LINGUAS),$(lc)/$(PROJECT).po)
MOFILES = $(patsubst %.po,%.mo,$(POFILES))
LOCALIZED_FILES = $(foreach lc,$(LINGUAS),$(lc)/$(PROJECT).svg)
RASTERIZED_FILES = $(foreach i,0 1 2 3, $(subst .svg,-$(i).png,$(LOCALIZED_FILES)))
FONT_FILES = $(foreach lc,$(LINGUAS),$(lc)/OpenSans-Bold.ttf)
VERSION = $(shell git log -1 --date=short --format="format:%ad-%h")
ARCHIVE = tor-and-https-$(VERSION).tar.gz

all: svg repo $(LOCALIZED_FILES) $(RASTERIZED_FILES)

.PHONY: svg
svg: C/$(PROJECT).svg

C/$(PROJECT).svg: make-interactive.xsl $(PROJECT)-inkscape.svg
	mkdir -p C
	xsltproc make-interactive.xsl $(PROJECT)-inkscape.svg > $@

$(POTFILE): C/$(PROJECT).svg
	itstool -o $@ $<

.PHONY: repo
repo: $(POFILES)

%.po: $(POTFILE)
	msgmerge --update --quiet --no-wrap --no-location $@ $<
	touch $@

%.mo: %.po
	msgfmt -o $@ $<

ar/$(PROJECT).svg: ar/DroidNaskh-Bold.ttf
fa/$(PROJECT).svg: fa/DroidNaskh-Bold.ttf
gr/$(PROJECT).svg: gr/DroidSans-Bold.ttf
km/$(PROJECT).svg: km/NotoSansKhmer-Bold.ttf
%/$(PROJECT).svg: %/$(PROJECT).mo %/OpenSans-Bold.ttf C/$(PROJECT).svg
	itstool -m $< -o $@ C/$(PROJECT).svg
	sed -e "s/^<svg /\0xml:lang='$$(dirname $@ | sed -e 's/^\(..\)/\1/')\' /" -i $@

.PHONY: raster
raster: $(RASTERIZED_FILES)

%/$(PROJECT)-0.png %/$(PROJECT)-1.png %/$(PROJECT)-2.png %/$(PROJECT)-3.png: %/$(PROJECT)-phantomjs.svg rasterize.js
	phantomjs rasterize.js $(shell dirname $<)

%/$(PROJECT)-phantomjs.svg: %/$(PROJECT).svg mangle-font-face.sed
	sed -f mangle-font-face.sed < $< > $@

%/OpenSans-Bold.ttf: OpenSans-Bold.ttf
	cp -f $< $@

%/DroidNaskh-Bold: DroidNaskh-Bold.ttf
	cp -f $< $@

%/DroidSans-Bold.ttf: DroidSans-Bold.ttf
	cp -f $< $@

%/NotoSansKhmer-Bold.ttf: NotoSansKhmer-Bold.ttf
	cp -f $< $@

.PHONY: dist
dist: src/$(ARCHIVE)

src/$(ARCHIVE): .git/logs/HEAD
	mkdir -p src
	git archive --format=tar --prefix=$(ARCHIVE)/ HEAD | gzip -9 > $@

.PHONY: publish
publish: dist $(LOCALIZED_FILES) $(RASTERIZED_FILES) $(FONT_FILES)
	rsync -av --exclude='*.po' --exclude='*.mo' --delete-after \
		README \
		LICENSE CC-BY-3.0 Apache-2.0 \
		$(LINGUAS) \
		src \
		perdulce.torproject.org:public_html/tor-and-https/

<!-- “Tor and HTTPS”
   - Copyright © 2012 Electronic Frontier Foundation
   -           © 2014 Lunar <lunar@torproject.org>
   -           © 2014 The Tor Project, Inc.
   - Licensed under CC BY 3.0
   -->
<xsl:stylesheet version="1.0"
 xmlns="http://www.w3.org/2000/svg"
 xmlns:svg="http://www.w3.org/2000/svg"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:cc="http://creativecommons.org/ns#"
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 xmlns:its="http://www.w3.org/2005/11/its"
 xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
 xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- copy all nodes by default -->
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <!-- clean up Inkscape specific attributes -->
  <xsl:template match="@inkscape:*" />
  <xsl:template match="inkscape:*" />
  <xsl:template match="@sodipodi:*" />
  <xsl:template match="sodipodi:*" />

  <!-- mark medata as untranslatable -->
  <xsl:template match="svg:metadata">
    <xsl:copy>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="svg:svg">
    <svg>
      <!-- add script -->
      <xsl:attribute name="onload">
        <xsl:text>init()</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="direction">
        <xsl:text>ltr</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@*"/>
      <its:rules version="1.0">
        <its:translateRule selector="//@direction" translate="yes"/>
        <its:locNoteRule selector="//@direction">
	  <its:locNote>
            Specify "ltr" for left-to-right languages or "rtl"
            for right-to-left languages (e.g. Arabic or Hebrew).
          </its:locNote>
        </its:locNoteRule>
        <its:locNoteRule selector="//svg:defs/svg:text[@id = 'string-site' or @id = 'string-login' or
                                                           @id = 'string-data' or @id = 'string-location']">
	  <its:locNote>
            Keep it short: 7em max. Seven times the capital letter "M".
          </its:locNote>
        </its:locNoteRule>
        <its:locNoteRule selector="//svg:defs/svg:text[@id = 'string-hacker' or @id = 'string-lawyer' or
                                                           @id = 'string-sysadmin' or @id = 'string-police' or
                                                           @id = 'string-tor-relay']">
	  <its:locNote>
            Keep it short: 8em is ok, 9em is max.
          </its:locNote>
        </its:locNoteRule>
        <its:locNoteRule selector="//svg:defs/svg:text[@id = 'string-wifi']">
	  <its:locNote>
            Keep it short: 3em max.
          </its:locNote>
        </its:locNoteRule>
        <its:locNoteRule selector="//svg:defs/svg:text[@id = 'string-isp']">
	  <its:locNote>
            Keep it short: 4em max.
          </its:locNote>
        </its:locNoteRule>
      </its:rules>
      <xsl:apply-templates select="*"/>
    </svg>
  </xsl:template>

  <!-- add text elements for translations -->
  <xsl:template match="svg:defs">
    <defs>
      <xsl:apply-templates select="@* | *"/>
      <script type="application/ecmascript" its:translate="no"><![CDATA[
const DISABLED_BUTTON_COLOR = "#a7a9ac";
const ENABLED_BUTTON_COLOR = "#a2c83a";
const UPPER_CASE_LABELS = /(hacker|lawyer|sysadmin|police|tor-relay|wifi|key)/;
const SPECIAL_LANGS = /(ar|gr|km|fa)/;

var buttonTor;
var buttonHTTPS;
var buttonTorBackground;
var buttonHTTPSBackground;
var layerTor;
var torEnabled;
var httpsEnabled;
var leaks;
var textLabels = { site: null, login: null, data: null, location: null, tor: null };

function init() {
    handleBidi();
    fillLabels();

    buttonTor = document.getElementById("button-tor");
    buttonHTTPS = document.getElementById("button-https");
    buttonTorBackground = document.getElementById("button-tor-background");
    buttonHTTPSBackground = document.getElementById("button-https-background");
    layerTor = document.getElementById("layer-tor");

    buttonTor.addEventListener('mouseover', createOpacityHandler(buttonTorBackground, 0.7));
    buttonTor.addEventListener('mouseout', createOpacityHandler(buttonTorBackground, 1.0));
    buttonTor.addEventListener('click', toggleTor);
    buttonHTTPS.addEventListener('mouseover', createOpacityHandler(buttonHTTPSBackground, 0.7));
    buttonHTTPS.addEventListener('mouseout', createOpacityHandler(buttonHTTPSBackground, 1.0));
    buttonHTTPS.addEventListener('click', toggleHTTPS);

    disableTor();
    disableHTTPS();
}

function createOpacityHandler(object, opacity) {
    return function(evt) {
        object.setAttribute('opacity', opacity);
    }
}

function handleBidi() {
    var svgObject = document.getElementsByTagName("svg")[0];
    if (svgObject.getAttribute("direction") != "rtl") {
        return;
    }

    var mirrorGObject = document.createElementNS("http://www.w3.org/2000/svg", "g");
    mirrorGObject.setAttribute("transform", "translate(" + svgObject.getAttribute("width") + ", 0) scale(-1, 1)");
    mirrorGObject.setAttribute("id", "mirror");
    var nextChild = svgObject.firstChild;
    for (var child = nextChild; child != null; child = nextChild) {
        nextChild = child.nextSibling;
        if (child.tagName != "g") {
            continue;
        }
        mirrorGObject.appendChild(child);
    }
    svgObject.appendChild(mirrorGObject);

    var elements = document.getElementsByTagName("text");
    for (elementIndex = 0; elementIndex < elements.length; elementIndex++) {
        var textObject = elements[elementIndex];
        if (textObject.parentNode.tagName == "defs") {
            continue;
        }
        var transform = textObject.getAttribute("transform") ? textObject.getAttribute("transform") : "";
        transform += " translate(" + (2 * parseFloat(textObject.getAttribute("x"))) + ", 0) scale(-1, 1)"
        textObject.setAttribute("transform", transform);
    }
}

function fillLabels() {

    for (var def = document.getElementsByTagName("defs")[0].firstChild; def != null; def = def.nextSibling) {
        if (def.tagName != "text") {
            continue;
        }
        var stringObject = def;
        var stringId = stringObject.id.substring("string-".length);
        var stringValue = stringObject.firstChild.data;
        if (stringId in textLabels) {
            textLabels[stringId] = stringValue;
        }

        if (stringId.match(UPPER_CASE_LABELS)) {
            stringValue = stringValue.toUpperCase();
        }
        var tspan = null;
        var elements = document.getElementsByTagName("tspan");
        for (elementIndex = 0; elementIndex < elements.length; elementIndex++) {
            if (elements[elementIndex].firstChild &&
                elements[elementIndex].firstChild.data == ("string-" + stringId)) {
                tspan = elements[elementIndex];
                if (!tspan) {
                    continue;
                }
                var textNode = document.createTextNode(stringValue);
                if (tspan.firstChild) {
                    tspan.replaceChild(textNode, tspan.firstChild);
                } else {
                    tspan.appendChild(textNode);
                }
                var labelId = tspan.parentNode.id.substring("label-".length);
                // adjust font size if needed
                var containerObject = document.getElementById("container-" + labelId);
                if (containerObject) {
                    var textObject = tspan.parentNode;
                    var originalHeight = textObject.getBBox().height;
                    var containerWidth = containerObject.getBBox().width;
                    while (true) {
                        var textWidth = tspan.getComputedTextLength();
                        if (textWidth < containerWidth * 0.95) {
                            break;
                        }
                        var unit = textObject.style.fontSize.match(/[a-z]*$/);
                        var size = textObject.style.fontSize.match(/^[0-9]*/);
                        textObject.style.fontSize = (size - 1) + unit;
                        if (textWidth == tspan.getComputedTextLength()) {
                            console.log("text not changing size, something is wrong");
                            break;
                        }
                    }
                    var newHeight = textObject.getBBox().height;
                    textObject.setAttribute("transform",
                        textObject.getAttribute("transform") +
                        " translate(0, " + ((newHeight - originalHeight) / 4) + ")");
                }
            }
        }
    }

    leaks = { withoutTorAndWithoutHTTPS:
              [ { name: "user",                 labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "hacker",               labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "lawyer-user-isp",      labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "sysadmin-user-isp",    labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "police-user-isp",      labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "nsa-user-isp",         labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "nsa-website-isp",      labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "website-isp",          labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "police-website-isp",   labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "sysadmin-website-isp", labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
                { name: "lawyer-website-isp",   labels: { site: true,  login: true,  data: true,  location: true,  tor: false } },
              ],
              withoutTorButWithHTTPS:
              [ { name: "user",                 labels: { site: true,  login: true,  data: true,  location: true, tor: false } },
                { name: "hacker",               labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "lawyer-user-isp",      labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "sysadmin-user-isp",    labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "police-user-isp",      labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "nsa-user-isp",         labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "nsa-website-isp",      labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "website-isp",          labels: { site: true,  login: false, data: false, location: true, tor: false } },
                { name: "police-website-isp",   labels: { site: true,  login: true,  data: true,  location: true, tor: false } },
                { name: "sysadmin-website-isp", labels: { site: true,  login: true,  data: true,  location: true, tor: false } },
                { name: "lawyer-website-isp",   labels: { site: true,  login: true,  data: true,  location: true, tor: false } },
              ],
              withoutHTTPSButWithTor:
              [ { name: "user",                 labels: { site: true,  login: true,  data: true,  location: true,  tor: true  } },
                { name: "hacker",               labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "lawyer-user-isp",      labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "sysadmin-user-isp",    labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "police-user-isp",      labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "nsa-user-isp",         labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "tor-guard-node",       labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "tor-middle-node",      labels: { site: false, login: false, data: false, location: false, tor: true  } },
                { name: "tor-exit-node",        labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "nsa-website-isp",      labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "website-isp",          labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "police-website-isp",   labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "sysadmin-website-isp", labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "lawyer-website-isp",   labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
              ],
              withBothTorAndHTTPS:
              [ { name: "user",                 labels: { site: true,  login: true,  data: true,  location: true,  tor: true  } },
                { name: "hacker",               labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "lawyer-user-isp",      labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "sysadmin-user-isp",    labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "police-user-isp",      labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "nsa-user-isp",         labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "tor-guard-node",       labels: { site: false, login: false, data: false, location: true,  tor: true  } },
                { name: "tor-middle-node",      labels: { site: false, login: false, data: false, location: false, tor: true  } },
                { name: "tor-exit-node",        labels: { site: true,  login: false, data: false, location: false, tor: true  } },
                { name: "nsa-website-isp",      labels: { site: true,  login: false, data: false, location: false, tor: true  } },
                { name: "website-isp",          labels: { site: true,  login: false, data: false, location: false, tor: true  } },
                { name: "police-website-isp",   labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "sysadmin-website-isp", labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
                { name: "lawyer-website-isp",   labels: { site: true,  login: true,  data: true,  location: false, tor: true  } },
              ],
            };


}

function toggleTor() {
    if (torEnabled) {
        disableTor();
    } else {
        enableTor();
    }
}

function enableTor() {
    torEnabled = true;
    buttonTorBackground.style.fill = ENABLED_BUTTON_COLOR;
    layerTor.style.display = "inherit";
    refreshTexts();
}

function disableTor() {
    torEnabled = false;
    buttonTorBackground.style.fill = DISABLED_BUTTON_COLOR;
    layerTor.style.display = "none";
    refreshTexts();
}

function toggleHTTPS() {
    if (httpsEnabled) {
        disableHTTPS();
    } else {
        enableHTTPS();
    }
}

function enableHTTPS() {
    httpsEnabled = true;
    buttonHTTPSBackground.style.fill = ENABLED_BUTTON_COLOR;
    refreshTexts();
}

function disableHTTPS() {
    httpsEnabled = false;
    buttonHTTPSBackground.style.fill = DISABLED_BUTTON_COLOR;
    refreshTexts();
}

function fixLang() {
    var svgObject = document.getElementsByTagName("svg")[0];
    if (!svgObject.getAttributeNS("http://www.w3.org/XML/1998/namespace", "lang").match(SPECIAL_LANGS)) {
        return;
    }

    var elements = document.getElementsByTagName("text");
    for (elementIndex = 0; elementIndex < elements.length; elementIndex++) {
        var textObject = elements[elementIndex];
        if (textObject.parentNode.tagName == "defs") {
            continue;
        }
        // use Latin font for pure latin
        if (textObject.childNodes.length == 1 && textObject.childNodes[0].tagName == "tspan") {
            if (textObject.childNodes[0].textContent.match(/^[A-z.\/ ]+$/)) {
                textObject.setAttributeNS("http://www.w3.org/XML/1998/namespace", "lang", "en");
            }
        }
    }
}

function refreshTexts() {
    var texts;

    if (!torEnabled && !httpsEnabled) {
        texts = leaks.withoutTorAndWithoutHTTPS;
    } else if (!torEnabled && httpsEnabled) {
        texts = leaks.withoutTorButWithHTTPS;
    } else if (torEnabled && !httpsEnabled) {
        texts = leaks.withoutHTTPSButWithTor;
    } else if (torEnabled && httpsEnabled) {
        texts = leaks.withBothTorAndHTTPS;
    }

    for (var textIndex = 0; textIndex < texts.length; textIndex++) {
        var labels = texts[textIndex].labels
        for (var label in labels) {
            if (!labels.hasOwnProperty(label)) {
                continue;
            }
            var textObject = document.getElementById("text-" + texts[textIndex].name + "-" + label);
            var tspan = textObject.firstChild; // ought to be a <tspan/>
            var text;
            if (labels[label]) {
                text = textLabels[label].toUpperCase();
            } else {
                text = label == "tor" ? " " : "…";
            }
            // This ought to be a text node
            if (tspan.firstChild) {
                tspan.firstChild.data = text;
            } else {
                var textNode = document.createTextNode(text);
                tspan.appendChild(textNode);
            }
        }
    }
    fixLang();
}
]]></script>
      <!-- add style -->
      <style type="text/css" its:translate="no"><![CDATA[
@font-face {
  font-family: OpenSansBold;
  font-style: normal;
  font-weight: 700;
  src: url(OpenSans-Bold.ttf) format('truetype');
}

@font-face {
    font-family: DroidSansBold;
    font-style: normal;
    font-weight: 700;
    src: url(DroidSans-Bold.ttf) format('truetype');
}

@font-face {
    font-family: NotoSansKhmerBold;
    font-style: normal;
    font-weight: 700;
    src: url(NotoSansKhmer-Bold.ttf) format('truetype');
}

text {
    font-family: OpenSansBold;
    font-weight: bold;
}

:lang(ar) {
    font-family: sans-serif;
    font-weight: normal;
}

:lang(fa) {
    font-family: sans-serif;
    font-weight: normal;
}

:lang(el) {
    font-family: DroidSansBold;
    font-weight: bold;
}

:lang(km) {
    font-family: NotoSansKhmerBold;
    font-weight: bold;
}

#button-tor, #button-https {
  cursor: pointer;
}
]]></style>
      <text id="string-site">Site.com</text>
      <text id="string-login">user / pw</text>
      <text id="string-data">data</text>
      <text id="string-location">location</text>
      <text id="string-wifi">WiFi</text>
      <text id="string-isp">ISP</text>
      <text id="string-hacker">Hacker</text>
      <text id="string-lawyer">Lawyer</text>
      <text id="string-sysadmin">Sysadmin</text>
      <text id="string-police">Police</text>
      <text id="string-nsa">NSA</text>
      <text id="string-tor-relay">Tor relay</text>
      <text id="string-key">Key</text>
      <text id="string-internet-connection">Internet connection</text>
      <text id="string-eavesdropping">Eavesdropping</text>
      <text id="string-data-sharing">Data sharing</text>
      <text its:translate="no" id="string-tor">Tor</text>
      <text its:translate="no" id="string-https">HTTPS</text>
    </defs>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'Site.com' or text() = 'SITE.COM']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-site</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'USER / PW']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-login</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'DATA']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-data</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'LOCATION']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-location</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'ISP']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-isp</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'HACKER']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-hacker</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'LAWYER']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-lawyer</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'SYSADMIN']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-sysadmin</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'POLICE']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-police</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'NSA']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-nsa</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'TOR RELAY']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-tor-relay</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'KEY']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-key</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'Internet connection']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-internet-connection</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'Eavesdropping']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-eavesdropping</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'Data sharing']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-data-sharing</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'Tor']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-tor</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'HTTPS']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-https</xsl:text>
    </tspan>
  </xsl:template>
  <xsl:template match="//svg:tspan[text() = 'WIFI']">
    <tspan>
      <xsl:attribute name="its:translate">
        <xsl:text>no</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@* | *"/>
      <xsl:text>string-wifi</xsl:text>
    </tspan>
  </xsl:template>

  <!-- remove text from leak frames -->
  <xsl:template match="//svg:text[starts-with(@id, 'text-')]/svg:tspan">
    <xsl:copy>
      <xsl:apply-templates select="@* | *"/>
    </xsl:copy>
  </xsl:template>

  <!-- remove font-family from text style attributes -->
  <xsl:template match="svg:text">
    <xsl:param name="style" select="@style" />
    <xsl:param name="style-without-font-family">
      <xsl:value-of select="substring-before($style, 'font-family:')" />
      <xsl:value-of select="substring-after(substring-after($style, 'font-family:'), ';')" />
    </xsl:param>
    <xsl:param name="style-cleaned">
      <xsl:value-of select="substring-before($style-without-font-family, 'font-weight:')" />
      <xsl:value-of select="substring-after(substring-after($style-without-font-family, 'font-weight:'), ';')" />
    </xsl:param>
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="style">
        <xsl:value-of select="$style-cleaned" />
      </xsl:attribute>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

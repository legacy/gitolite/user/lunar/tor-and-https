# “Tor and HTTPS”
# Copyright © 2014 Lunar <lunar@torproject.org>
#           © 2014 The Tor Project, Inc.
# Licensed under CC BY 3.0

# The following will replace the original font-face by one that
# PhantomJS is happy to render.
/^@font-face {/,/}/ {
  s/font-weight:.*/font-weight: normal;/
}

/^text {/i \
  @font-face { \
      font-family: DroidArabicNaskh; \
      font-style: normal; \
      font-weight: normal; \
      src: url(DroidNaskh-Bold.ttf) format('truetype'); \
  }

/^:lang(ar) {/,/}/ {
  s/font-family:.*/font-family: DroidArabicNaskh;/
}
